particle check template
===========================

This project is intended as a template for doing automated tests of code to be used with [particle.io](https://particle.io) boron boards.

We use [cppcheck](https://sourceforge.net/projects/cppcheck/) for static check of code. It can check for at lot of things, see their [docs](https://sourceforge.net/p/cppcheck/wiki/ListOfChecks/)

Particle.io uses it internally also, see [here](https://docs.platformio.org/en/latest/plus/check-tools/cppcheck.html).

Static check usage
----------------------

Use `check_ino.sh` to check .ino files and `check.cpp` to check cpp files.

We rely on the particle-cli module. Go [here](https://docs.particle.io/reference/developer-tools/cli/) for more info. The preprocess subcommand is used to convert .ino files to .cpp files (see `particle preprocess --help` for details)

Doxygen documentation
-------------------

As part of the pipeline we added [doxygen](http://www.doxygen.nl/index.html) documentation generation.

Go [here](http://www.doxygen.nl/manual/docblocks.html) for how to document your code.

We suggest using two ways of documenting.

For brief one-liners
```cpp
/// internal counter to output something that changes predictably
int counter = 0;
```

and for when detailed description is neededM
```cpp
/**
\brief The LED to blink

D7 is the internal diode.
*/
int led_onboard = D7;
```

The examples are for variables, but are applicable for functions as well.
