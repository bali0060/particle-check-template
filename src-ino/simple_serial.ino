// this is a program to test serial connection
// notice it uses Serial1 which is the serial connection on the I/O pins, not the one using USB

/// internal counter to output something that changes predictably
int counter = 0;

/**
\brief The LED to blink

D7 is the internal diode.
*/
int led_onboard = D7;

/**
\brief Initial setup (called by particle framework).

(and here goes some more detailed info)
*/
void setup() {
	Serial1.begin(9600);

	pinMode(led_onboard, OUTPUT);
}

/// Main loop (called by particle framework)
void loop() {
	Serial1.printlnf("H testing %d", ++counter);
	digitalWrite(led_onboard, HIGH);
	delay(1000);

	Serial1.printlnf("L testing %d", ++counter);
	digitalWrite(led_onboard, LOW);
	delay(1000);
}
